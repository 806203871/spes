<?php

return [
    // 模块化
    'module'     => [
        'root'      => app_path('Modules'),
        'namespace' => 'App\\Modules',
        // 文件夹映射
        'dirs'      => [
            'controller' => 'Controllers',
            'event'      => 'Events',
            'listener'   => 'Listeners',
            'command'    => 'Commands',
            'component'  => 'Components',    // .php
            'view'       => 'Views',         // .blade.php
            'request'    => 'Requests',
            'model'      => 'Models',
            'schedule'   => 'Schedules',
            // ... 其他类型文件夹映射
        ],

        'generator' => [
            'dirs' => [
                'Controllers',
                'Events',
                'Listeners',
                'Commands',
                'Components',
                'Views',
                'Requests',
                'Models',
                'Schedules'
            ],
//            'files' => [
//                'routes.php'
//            ]
            // ... 其他需要生成的文件夹类型
        ],

        // 模块内容加载器
        'loaders'   => [
            \Simanx\Spes\Loader\RouteLoader::class    => [
                'dir' => 'controller'
            ],
            \Simanx\Spes\Loader\EventLoader::class    => [
                'dir' => 'listener'
            ],
            \Simanx\Spes\Loader\CommandLoader::class  => [
                'dir' => 'command'
            ],
            \Simanx\Spes\Loader\ScheduleLoader::class => [
                'dir' => 'schedule'
            ],
            // 更多的loader
        ],
        // 是否使用view，如果只是作为api提供，则通常不需要view
        'view'      => true,
    ],

    'loaders' => [
//        \Simanx\Spes\Loader\CommandLoader::class  => [
//            'path'   => [
//                app_path('Console/Commands/')
//            ],
//            'suffix' => 'Command.php',
//            'name' => '<CommandName.php>'
//        ],
        \Simanx\Spes\Loader\ScheduleLoader::class => [
            'path' => [
                app_path('Console/Schedules/')
            ]
        ]
    ]
];
