<?php

namespace Simanx\Spes\Loader;

use Illuminate\Console\Scheduling\Schedule as LaravelSchedule;
use Simanx\Spes\Attribute\ClassAttributeProxy;
use Simanx\Spes\Attribute\Command\Attributes\Cron;

class ScheduleLoader extends Loader
{
    public function load()
    {
        if (!$this->app->runningInConsole()) {
            return;
        }

        foreach ($this->classes() as $class) {
            $classAttrProxy = ClassAttributeProxy::create($class);
            /** @var Cron $schedule */
            $schedule = $classAttrProxy->getAttribute(Cron::class);
            if (!$schedule) {
                continue;
            }

            /** @var LaravelSchedule $laravelSchedule */
            $laravelSchedule = app(LaravelSchedule::class);
            $laravelSchedule->command($classAttrProxy->getClassName())->cron($schedule->cron);
        }
    }
}