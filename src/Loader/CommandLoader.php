<?php

namespace Simanx\Spes\Loader;

use Illuminate\Console\Application as Artisan;
use Simanx\Spes\Attribute\ClassAttributeProxy;
use Simanx\Spes\Attribute\Command\Attributes\ArtisanCommand;

/**
 * 命令加载工具
 * @package Simanx\Spes\Command
 */
class CommandLoader extends Loader
{
    public function load()
    {
        if (!$this->app->runningInConsole()) {
            return;
        }

        foreach ($this->classes() as $class) {
            $classAttrProxy = ClassAttributeProxy::create($class);
            $commandAttr = $classAttrProxy->getAttribute(ArtisanCommand::class);
            if (!$commandAttr) {
                continue;
            }

            Artisan::starting(function ($artisan) use ($classAttrProxy) {
                $artisan->resolveCommands($classAttrProxy->getClassName());
            });
        }
    }
}