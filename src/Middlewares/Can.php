<?php

namespace Simanx\Spes\Middlewares;

use Closure;
use Illuminate\Contracts\Auth\Access\Gate;

class Can
{
    protected $gate;

    public function __construct(Gate $gate)
    {
        $this->gate = $gate;
    }

    public function handle($request, Closure $next, $ability, ...$arguments)
    {
        $this->gate->authorize($ability, $arguments);
        return $next($request);
    }
}