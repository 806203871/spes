<?php

namespace Simanx\Spes\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [];

    public function load($events)
    {
        if ($this->app->isBooted()) {
            return;
        }

        foreach ($events as $event) {
            if (isset($this->listen[$event['event']])) {
                $this->listen[$event['event']] = [];
            }

            $this->listen[$event['event']][] = $event['listener'];
        }
    }
}
