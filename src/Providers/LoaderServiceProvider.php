<?php

namespace Simanx\Spes\Providers;

use Illuminate\Support\ServiceProvider;
use Simanx\Spes\Loader\Loader;

/**
 * 加载器的服务提供者
 * @package Simanx\Spes\Providers
 */
class LoaderServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $loaders = config('spes.loaders', []);
        foreach ($loaders as $loader => $config) {
            /** @var Loader $loader */
            $loader = app($loader, [
                'app'    => $this->app,
                'config' => $config
            ]);

            $loader->load();
        }
    }
}