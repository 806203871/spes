<?php

namespace Simanx\Spes\Model;

use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{
    /**
     * 允许批量赋值
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * @param array $attributes
     * @param bool $skipArrayAttr
     * @return Model
     */
    public function setAttributes(array $attributes, $skipArrayAttr = false)
    {
        foreach ($attributes as $key => $value) {
            if ($skipArrayAttr && is_array($value)) {
                continue;
            }

            $this->setAttribute($key, $value);
        }

        return $this;
    }

    /**
     * 插入数据
     * @param array $values
     * @return bool
     */
    public static function insert(array $values)
    {
        return \Illuminate\Support\Facades\DB::table((new static)->getTable())
            ->insert($values);
    }
}