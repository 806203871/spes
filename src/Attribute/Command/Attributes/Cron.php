<?php

namespace Simanx\Spes\Attribute\Command\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
class Cron
{
    public function __construct(public string $cron)
    {
    }
}