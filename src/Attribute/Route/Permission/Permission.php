<?php

namespace Simanx\Spes\Attribute\Route\Permission;

use Illuminate\Contracts\Support\Arrayable;

class Permission implements Arrayable
{
    /**
     * 权限标识
     * @var string
     */
    public string $permission;

    /**
     * 权限名称
     * @var string
     */
    public string $name;

    /**
     * 额外参数
     * @var array
     */
    public array $options;

    /**
     * Permission constructor.
     * @param string $permission
     * @param string|null $name
     * @param array $options
     */
    public function __construct(string $permission, string $name = null, array $options = [])
    {
        $this->permission = $permission;
        $this->name = $name ?? $permission;
        $this->options = $options;
    }

    /**
     * 权限标识
     * @return string
     */
    public function permission()
    {
        return $this->permission;
    }

    /**
     * 权限名称，用于显示
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    public function toArray()
    {
        return [
            'name'       => $this->name,
            'permission' => $this->permission,
            'options'    => $this->options
        ];
    }
}