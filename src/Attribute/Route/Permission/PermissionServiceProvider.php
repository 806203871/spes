<?php

namespace Simanx\Spes\Attribute\Route\Permission;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Simanx\Spes\Attribute\Route\Permission\Contracts\HasPermission;

class PermissionServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(PermissionManager::class);

        Gate::before(function (Authenticatable $user, string $ability, $arguments) {
            if ($ability != '__SPES__' || !$user instanceof HasPermission) {
                return;
            }

            return $user->can($arguments[0]);
        });
    }
}