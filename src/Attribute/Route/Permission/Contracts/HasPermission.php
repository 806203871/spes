<?php

namespace Simanx\Spes\Attribute\Route\Permission\Contracts;

interface HasPermission
{
    public function getPermissions();

    public function can($permission);

    public function cannot($permission);
}