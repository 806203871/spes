<?php

namespace Simanx\Spes\Attribute\Route\Permission\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD)]
class Can
{
    /**
     * 权限别名
     * @var string
     */
    public string $name;

    /**
     * 其他选项
     * @var array
     */
    public array $options;

    public function __construct(public string $permission, string $name = null, array $options = [])
    {
        $this->name = $name ?? $permission;
        $this->options = $options;
    }
}