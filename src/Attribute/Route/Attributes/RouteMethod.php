<?php

namespace Simanx\Spes\Attribute\Route\Attributes;

use Illuminate\Support\Arr;

// 方法注册路由基类
abstract class RouteMethod
{
    public string $method;

    public string|array $middleware;

    /**
     * RouteMethod constructor.
     * @param string $path 路由路径
     * @param string $name 路由名称
     * @param string|array $middleware 中间件
     * @param array $action 额外action参数, domain, namespace, prefix, as, uses, controller, missing
     */
    public function __construct(
        public string $path = '',
        public string $name = '',
        string|array $middleware = [],
        public array $action = [])
    {
        $this->method = $this->getMethod();
        $this->middleware = Arr::wrap($middleware);
    }

    public abstract function getMethod(): string;
}