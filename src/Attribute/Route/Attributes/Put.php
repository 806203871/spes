<?php

namespace Simanx\Spes\Attribute\Route\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_METHOD)]
class Put extends RouteMethod
{
    public function getMethod(): string
    {
        return 'PUT';
    }
}