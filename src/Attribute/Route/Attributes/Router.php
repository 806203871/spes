<?php

namespace Simanx\Spes\Attribute\Route\Attributes;

use Illuminate\Support\Arr;

#[\Attribute(\Attribute::TARGET_CLASS)]
class Router
{
    public array $middleware = [];

    public function __construct(
        public string $path = '',
        public string $name = '',
        string|array $middleware = [])
    {
        $this->middleware = Arr::wrap($middleware);
    }
}