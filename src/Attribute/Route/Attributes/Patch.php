<?php

namespace Simanx\Spes\Attribute\Route\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_METHOD)]
class Patch extends RouteMethod
{
    public function getMethod(): string
    {
        return 'PATCH';
    }
}