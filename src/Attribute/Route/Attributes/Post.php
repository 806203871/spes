<?php

namespace Simanx\Spes\Attribute\Route\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_METHOD)]
class Post extends RouteMethod
{
    public function getMethod(): string
    {
        return 'POST';
    }
}