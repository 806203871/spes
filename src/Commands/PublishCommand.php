<?php

namespace Simanx\Spes\Commands;

use Illuminate\Console\Command;

class PublishCommand extends Command
{
    protected $signature = 'spes:publish {--force}';

    protected $description = '发布spes资源';

    public function handle()
    {
        $force = $this->option('force');
        $options = [
            '--provider' => 'Simanx\Spes\Providers\SpesServiceProvider'
        ];

        if ($force === true) {
            $options['--force'] = true;
        }

        $this->call('vendor:publish', $options);
    }
}