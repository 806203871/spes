<?php

namespace Simanx\Spes\Commands;

use Illuminate\Console\Command;
use Simanx\Spes\Attribute\Route\Permission\PermissionManager;

class PermissionClearCommand extends Command
{
    protected $signature = 'spes:permission-clear';

    protected $description = '删除权限缓存';

    public function handle()
    {
        /** @var PermissionManager $permissionManager */
        $permissionManager = app(PermissionManager::class);
        app('files')->delete($permissionManager->getCachePath());
        $this->info('权限缓存已清除');
    }
}