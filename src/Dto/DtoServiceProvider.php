<?php

namespace Simanx\Spes\Dto;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

/**
 * 文件传输对象ServiceProvider
 * @package Simanx\Spes\Dto
 */
class DtoServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->beforeResolving(Dto::class, function ($class, $params, $app) {
            $this->app->bindIf($class, function ($app, $params) use ($class) {
                /** @var Request $request */
                $request = app('request');
                /** @var DtoResolver $resolver */
                $resolver = $app->make(DtoResolver::class);
                $dto = $resolver->resolve($class, $request);
                $dto->validate();
                return $dto;
            });
        });
    }
}