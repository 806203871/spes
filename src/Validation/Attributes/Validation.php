<?php

namespace Simanx\Spes\Validation\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_PROPERTY)]
class Validation
{
    public string $message;
    public string $rule;

    public function __construct(string $rule, string $message = '')
    {
        $this->rule = $rule;
        $this->message = $message;
    }
}